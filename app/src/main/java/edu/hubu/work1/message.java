package edu.hubu.work1;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class message extends Fragment{

    View view;
    Myadapter adapter;
    RecyclerView recyclerView;
    List<String> list;

    public message() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.message, container, false);
        recyclerView = view.findViewById(R.id.recyclerview);
        ArrayList<String> arraylist1 = new ArrayList<String>(Arrays.asList("Nilu","Paimon","Aether","Lumine","Eula","Venti","Barbara","Noelle","Qiqi","Keqing","Zhongli"));
        list = new ArrayList<String>(arraylist1);

        Myadapter myadapter = new Myadapter(getContext(), (ArrayList<String>) list);
        recyclerView.setAdapter(myadapter);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(manager);
        return view;
    }
}
