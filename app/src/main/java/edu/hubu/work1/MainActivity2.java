package edu.hubu.work1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

public class MainActivity2 extends AppCompatActivity {

    Fragment message,friend,find,mine;
    FragmentManager manager;
    int transaction;
    private LinearLayout Linearlayout1,Linearlayout2,Linearlayout3,Linearlayout4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Linearlayout1 = findViewById(R.id.tab_message);
        Linearlayout2=findViewById(R.id.tab_friend);
        Linearlayout3=findViewById(R.id.tab_find);
        Linearlayout4=findViewById(R.id.tab_mine);

        manager=getSupportFragmentManager();

        message=new message();
        friend=new friend();
        find=new find();
        mine=new mine();

        Frags_init();
        Frags_hide();
        Frags_show(message);

        Linearlayout1.setOnClickListener(this::onClick);
        Linearlayout2.setOnClickListener(this::onClick);
        Linearlayout3.setOnClickListener(this::onClick);
        Linearlayout4.setOnClickListener(this::onClick);

    }

    private void Frags_init()
    {
        transaction= manager.beginTransaction()
                .add(R.id.content,message)
                .add(R.id.content,friend)
                .add(R.id.content,find)
                .add(R.id.content,mine)
                .commit();
    }

    public void Frags_hide()
    {
        transaction=manager.beginTransaction()
                .hide(message)
                .hide(friend)
                .hide(find)
                .hide(mine)
                .commit();
    }

    private void Frags_show(Fragment f) {
        transaction=manager.beginTransaction()
                .show(f)
                .commit();
    }

    public void onClick(View v){
        Frags_hide();
        if (v.getId()==R.id.tab_message)
            Frags_show(message);
        else if (v.getId()==R.id.tab_friend)
            Frags_show(friend);
        else if (v.getId()==R.id.tab_find)
            Frags_show(find);
        else  if (v.getId()==R.id.tab_mine)
            Frags_show(mine);
    }
}