package edu.hubu.work1;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class Myadapter extends RecyclerView.Adapter<Myadapter.Myholder> {

    Context context1;
    List<String> list1;

    public Myadapter(Context context,List list){
        context1=context;
        list1=list;
    }

    @NonNull
    @Override
    public Myholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view=LayoutInflater.from(context1).inflate(R.layout.item,parent,false);

        Myholder holder=new Myholder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull Myholder holder, int position) {

        holder.textView1.setText(list1.get(position));
        final String content = list1.get(position);
        holder.itemView.setContentDescription(content);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context1, "您点击的联系人是：" + content, Toast.LENGTH_SHORT).show();


                Intent intent = new Intent();
                        intent.setClass(context1,niluactivity.class);


                context1.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {

        return list1.size();
    }

    public class Myholder extends RecyclerView.ViewHolder{

        TextView textView1;
        public  Myholder(@NonNull View itemview ){
            super(itemview);
            textView1=itemview.findViewById(R.id.textView9);
        }
    }
}